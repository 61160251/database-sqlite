/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author User
 */
public class Stock {
    private int id ;
    private String name ;
    private int amount ;
    private String date ;

    public void setDate(String date) {
         this.date = date;
    }

    public String    getDate() {
        return date;
    }
  
    
    public Stock(int id, String name, int amount,String date) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", amount=" + amount + '}';
    }

    
}
